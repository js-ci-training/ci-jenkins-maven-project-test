
package com.ci.jenkins.simpleapp.controller;

import com.ci.jenkins.simpleapp.model.AObj;
import org.junit.Before;
import org.junit.Test;

public class AContainersTest {
    private AContainer aContainerT;
    @Before
    public void setUp() {
        aContainerT =new AContainer();

    }
    @Test
    public void testSetA3() {
        AObj a =new AObj("a1","a2",5);
        a.setA3(100);
        assert(100==a.getA3());
    }

    @Test
    public void addAobj() {
        AObj a=new AObj("keya","att2",5);
        aContainerT.addAobj(a);
        assert(true);
    }

    @Test
    public void getAobj() {
        AObj a=new AObj("keya","att2",5);
        aContainerT.addAobj(a);
        AObj b= aContainerT.getAobj("keya");
        assert(b.getA1().equals("keya"));
        
    }

}
