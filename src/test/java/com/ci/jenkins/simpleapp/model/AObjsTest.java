package com.ci.jenkins.simpleapp.model;

import org.junit.Test;

public class AObjsTest {
    @Test
    public void testSetA1() {
        AObj a =new AObj("a1","a2",5);
        a.setA1("b1");
        assert("b1".equals(a.getA1()));
    }

    @Test
    public void testSetA2() {
        AObj a =new AObj("a1","a2",5);
        a.setA2("b2");
        assert("b2".equals(a.getA2()));
    }

    @Test
    public void testSetA3() {
        AObj a =new AObj("a1","a2",5);
        a.setA3(100);
        assert(100==a.getA3());
    }
}
