package com.ci.jenkins.simpleapp.controller;

import com.ci.jenkins.simpleapp.model.AObj;

import java.util.HashMap;
import java.util.Map;

public class AContainer {
    private Map<String, AObj> acontainer;

    public AContainer() {
        this.acontainer=new HashMap<String, AObj>();
    }

    public void addAobj(AObj a){
        this.acontainer.put(a.getA1(),a);
    }

    public AObj getAobj(String a1){
        return this.acontainer.get(a1);
    }
}
